# Simple API service for alerts from Nagios ndo2db and Monitorscout.
# For use with mondash monitoring dashboard.

# Makes debugging easier
from pprint import pprint as pp

# Until pyvenv-3.4 is fixed on centos 7 support python 2.
try:
    from configparser import RawConfigParser
except ImportError:
    from ConfigParser import RawConfigParser

try:
    from xmlrpc.client import ServerProxy
except ImportError:
    from xmlrpclib import ServerProxy

from json import dumps
from datetime import datetime, time, timedelta
from base64 import b64decode

from logging import Formatter, getLogger, DEBUG, WARN, INFO
from logging.handlers import SysLogHandler, RotatingFileHandler

import pymysql as mysql
from pymysql.cursors import DictCursor

from bottle import default_app, route, post, run
from bottle import request, response, HTTPError
from bottle import static_file, template


config = RawConfigParser()
config.readfp(open('./mondash.cfg'))
config.read(['./mondash_local.cfg', '/etc/mondash.cfg'])

# Setup logging
formatter = Formatter(config.get('logging', 'log_format'))
l = getLogger('mondash')
if config.get('logging', 'log_handler') == 'syslog':
    syslog_address = config.get('logging', 'syslog_address')

    if syslog_address.startswith('/'):
        h = SysLogHandler(
            address=syslog_address,
            facility=SysLogHandler.LOG_LOCAL0
        )
    else:
        h = SysLogHandler(
            address=(
                config.get('logging', 'syslog_address'),
                config.get('logging', 'syslog_port')
            ),
            facility=SysLogHandler.LOG_LOCAL0
        )
else:
    h = RotatingFileHandler(
        config.get('logging', 'log_file'),
        maxBytes=config.getint('logging', 'log_max_bytes'),
        backupCount=config.getint('logging', 'log_max_copies')
    )
h.setFormatter(formatter)
l.addHandler(h)

if config.get('logging', 'log_debug'):
    l.setLevel(DEBUG)
else:
    l.setLevel(WARN)

# Get a list of api_account IDs
api_accounts = config.get('monitorscout', 'api_account').split(',')
ms_sessions = {}

monitorscout_enable = True
nagios_enable = True

try:
    ms_server = ServerProxy(
        config.get('monitorscout', 'api_url')
    )
except Exception as e:
    l.error('Monitorscout connection failed: {error}'.format(
        error=str(e)
    ))
    monitorscout_enable = False

for account_id in api_accounts:
    if not monitorscout_enable:
        continue

    account_id = account_id.strip(' ')
    _ms_sid = ms_server.login(
        config.get('monitorscout', 'api_username'),
        config.get('monitorscout', 'api_password'),
        account_id
    )

    l.info('Mondash web worker connected to MS API: {sid}'.format(
        sid=_ms_sid
    ))

    _org_data = ms_server.organisation.r_get2(_ms_sid)['entity_data']
    _org_id = list(_org_data.keys())[0]
    ms_sessions[_org_id] = {
        'sid': _ms_sid,
        'account_id': account_id
    }

try:
    # Connect to ndo2db database
    mysql_conn = mysql.connect(
        host=config.get('ndo2db', 'hostname'),
        user=config.get('ndo2db', 'username'),
        passwd=config.get('ndo2db', 'password'),
        db=config.get('ndo2db', 'database'),
        port=config.getint('ndo2db', 'port'),
        unix_socket=config.get('ndo2db', 'socket'),
        cursorclass=DictCursor
    )
except Exception as e:
    l.error('NDO connection failed: {error}'.format(
        error=str(e)
    ))
    nagios_enable = False


# Decorator to check if a user is read only and has no access to API
# methods. To find the username HTTP Basic auth is required, the
# Authorization header is used to get the username.
def require_write_access(fn):
    def check_access(**kw):
        try:
            username = get_username(request.headers.get('Authorization'))
        except Exception as e:
            l.exception(str(e))
            username = None
            pass
        
        # If there is no username we continue with API call assuming
        # simply that there is no HTTP Auth configured.
        if not username:
            l.debug('No username in Authorized header')
            return fn(**kw)

        response.content_type = 'application/json'
        ro_users = list_readonly_users()
        l.debug('Matching {user} against {users}'.format(
            user=username,
            users=ro_users
        ))

        # Otherwise deny a readonly user access to the API call.
        if username in ro_users:
            l.debug('User {user} blocked from API'.format(
                user=username
            ))
            raise HTTPError(
                status=401,
                body=dumps({
                    'error': 'Not authorized to use API'
                })
            )
        else:
            return fn(**kw)

    return check_access


def get_monitorscout_alerts(organisation, alert_types):
    if not monitorscout_enable:
        raise HTTPError(
            status=500,
            body='Monitorscout disabled'
        )

    org = ms_server.organisation.r_get2(
        ms_sessions[organisation].get('sid'),
    )
    active_alerts = ms_server.organisation.r_get_active_alerts(
        ms_sessions[organisation].get('sid'),
        '',
        100
    ).get('entity_data')

    alerts = []
    right_now = datetime.now()

    organisation_name = org['entity_data'][organisation]['name']

    for entity_id, entity_data in active_alerts.items():
        alert = {}
        if entity_data.get('type', None) not in alert_types:
            continue

        alert['alert_id'] = entity_id
        alert['organisation'] = organisation
        alert['organisation_name'] = organisation_name

        monitors = [
            'passive_monitor', 'monitor', 'process_monitor',
            'metric_monitor'
        ]
        if entity_data.get('type') in monitors:
            try:
                alert_data = active_alerts[entity_data.get('current_alert')]
            except KeyError as e:
                l.exception('current_alert was empty')
                continue

            alert['acked'] = alert_data.get('acked')
            alert['alert_id'] = alert_data.get('id')
            alert['output'] = alert_data.get('error_msg')
            alert['start_timestamp'] = alert_data.get('start_timestamp')
            alert['type'] = alert_data.get('type')
        elif entity_data.get('type') in ['device_alert']:
            alert['acked'] = entity_data.get('acked')
            alert['start_timestamp'] = entity_data.get('start_timestamp')
            alert['output'] = entity_data.get('error_msg')
            alert['type'] = entity_data.get('type')
        else:
            alert['acked'] = False

        alert['status'] = entity_data.get('status', '')

        alert['monitor_name'] = entity_data.get('name')
        alert['device'] = entity_data.get('device')

        alert_device = active_alerts[entity_data.get('device')]
        alert['hostname'] = alert_device.get('hostname')
        alert['object_id'] = alert_device.get('id')

        duration = right_now - datetime.fromtimestamp(alert['start_timestamp'])
        alert['duration'] = str(duration).split('.')[0]

        if entity_data.get('type') in alert_types and not alert.get('acked'):
            alerts.append(alert)

    return alerts


@route('/alert/monitorscout/<alert_type>')
def alert_monitorscout_alerts(alert_type):
    """
    This gets alerts for all organisations in ms_sessions and aggregates
    them.
    """

    if not monitorscout_enable:
        raise HTTPError(
            status=500,
            body='Monitorscout disabled'
        )

    response.content_type = 'application/json'

    ms_alerts = {
        'alerts_count': 0,
        'alerts': []
    }

    if alert_type == 'monitor':
        for org_id in ms_sessions.keys():
            _alerts = get_monitorscout_alerts(
                org_id,
                [
                    'passive_monitor',
                    'monitor',
                    'process_monitor',
                    'metric_monitor'
                ]
            )
            ms_alerts['alerts'].extend(_alerts)
            ms_alerts['alerts_count'] += len(_alerts)

    elif alert_type == 'device':
        for org_id in ms_sessions.keys():
            ms_alerts['alerts'].extend(get_monitorscout_alerts(
                org_id,
                ['device_alert']
            ))
    else:
        raise HTTPError(status=404, body='Unknown alert type')

    get_params = request.query_string.split('&')
    if 'pp' in get_params:
        return dumps(
            ms_alerts,
            sort_keys=True,
            indent=4
        )

    return dumps(ms_alerts)


# Handle monitorscout Device alerts, mostly just closes them.
@post('/alert/monitorscout/<organisation>/device/<alert_id>/close')
@require_write_access
def alert_monitorscout_device_handle(organisation, alert_id):
    l.debug('Closing Device alert: {alert}'.format(
        alert=alert_id
    ))

    try:
        ms_sid = ms_sessions[organisation].get('sid')
        ms_server.device.alert.r_close(ms_sid, alert_id)
    except Exception as e:
        l.debug('Caught exception closing device alert: {error}'.format(
            error=str(e)
        ))
        raise HTTPError(
            status=500,
            body=str(e)
        )


# Handle monitorscout passive/active monitor alerts by acknowleding them
@post('/alert/monitorscout/<organisation>/<alert_type>/<alert_id>/ack')
@require_write_access
def alert_monitorscout_monitor_handle(organisation, alert_type, alert_id):
    valid_types = ['passive_monitor_alert', 'monitor_alert']
    if alert_type not in valid_types:
        raise HTTPError(
            status=404,
            body='Invalid alert type'
        )

    ms_sid = ms_sessions[organisation].get('sid')

    if alert_type == 'passive_monitor_alert':
        try:
            ms_server.monitor.passive.alert.r_acknowledge(
                ms_sid,
                alert_id
            )
        except Exception as e:
            l.debug('Caught exception acking passive monitor'
                    ' alert: {error}'.format(
                        error=str(e)
                    )
                   )
            raise HTTPError(
                status=500,
                body=str(e)
            )
    if alert_type == 'monitor_alert':
        try:
            ms_server.monitor.alert.r_acknowledge(
                ms_sid,
                alert_id
            )
        except Exception as e:
            l.debug('Caught exception acking monitor alert: {error}'.format(
                error=str(e)
            ))
            raise HTTPError(
                status=500,
                body=str(e)
            )


def get_nagios_alerts(alert_type):
    if not nagios_enable:
        raise HTTPError(
            status=500,
            body='Nagios disabled'
        )

    nagios_status = [
        'OK',
        'WARNING',
        'CRITICAL',
        'UNKNOWN'
    ]

    cursor1 = mysql_conn.cursor()
    cursor2 = mysql_conn.cursor()

    alerts = []
    right_now = datetime.now()
    if alert_type == 'host':
        sql_query = '''
        select {prefix}hoststatus.hoststatus_id,
        {prefix}hoststatus.output,
        {prefix}hoststatus.current_state,
        {prefix}hoststatus.host_object_id,
        {prefix}hoststatus.last_time_up as last_time_up,
        {prefix}hoststatus.last_check as last_check,
        {prefix}hosts.display_name as hostname
        from {prefix}hoststatus, {prefix}hosts where
        {prefix}hosts.host_object_id={prefix}hoststatus.host_object_id and
        {prefix}hoststatus.current_state != 0 and
        {prefix}hoststatus.problem_has_been_acknowledged != 1 and
        {prefix}hosts.config_type=1
        '''.format(
            prefix=config.get('ndo2db', 'prefix')
        )
        cursor1.execute(sql_query)
        for row in cursor1:
            # Check for scheduled downtime
            sql_query = '''
            select author_name, comment_data, duration, 
            scheduled_start_time as scheduled_start_time,
            scheduled_end_time as scheduled_end_time, 
            actual_start_time as actual_start_time from {prefix}scheduleddowntime
            where object_id=%s order by entry_time desc limit 1
            '''.format(
                prefix=config.get('ndo2db', 'prefix')
            )
            cursor2.execute(sql_query, (row['host_object_id'],))
            if cursor2.rowcount >= 1:
                dt = cursor2.fetchone()
                if (dt['scheduled_start_time'] <= right_now and
                    dt['scheduled_end_time'] >= right_now):
                    continue

            # This handles some weird issue with Icinga creating zero'd date values
            if row['last_time_up'].startswith('0000'):
                row['last_time_up'] = '1970-01-01 00:00:00'

            last_time_up = datetime.strptime(row['last_time_up'], '%Y-%m-%d %H:%M:%S')

            duration = right_now - last_time_up
            alerts.append(
                {
                    'alert_id': row['hoststatus_id'],
                    'object_id': row['host_object_id'],
                    'duration': str(duration).split('.')[0],
                    'hoststatus_id': row['hoststatus_id'],
                    'output': row['output'],
                    'hostname': row['hostname'],
                    'state': row['current_state'],
                    'status': nagios_status[row['current_state']],
                    'host_object_id': row['host_object_id'],
                    'last_check': row['last_check'].strftime('%s')
                }
            )

    elif alert_type == 'service':
        sql_query = '''
        select {prefix}servicestatus.servicestatus_id,
        {prefix}servicestatus.output,
        {prefix}servicestatus.current_state,
        {prefix}servicestatus.service_object_id,
        {prefix}servicestatus.last_time_ok as last_time_ok,
        {prefix}servicestatus.last_check as last_check,
        {prefix}services.host_object_id,
        {prefix}services.display_name,
        {prefix}hosts.display_name as hostname
        from {prefix}servicestatus, {prefix}services, {prefix}hosts
        where current_state!=0 and
        problem_has_been_acknowledged!=1 and
        {prefix}servicestatus.service_object_id={prefix}services.service_object_id
        and {prefix}services.host_object_id={prefix}hosts.host_object_id
        and {prefix}services.config_type=1 and {prefix}hosts.config_type=1
        '''.format(
            prefix=config.get('ndo2db', 'prefix')
        )
        cursor1.execute(sql_query)
        for row in cursor1:
            # Check for scheduled downtime
            sql_query = '''
            select author_name, comment_data, duration, 
            scheduled_start_time as scheduled_start_time,
            scheduled_end_time as scheduled_end_time, 
            actual_start_time as actual_start_time from 
            {prefix}scheduleddowntime where object_id=%s order by entry_time desc limit 1
            '''.format(
                prefix=config.get('ndo2db', 'prefix')
            )
            cursor2.execute(sql_query, (row['service_object_id'],))
            if cursor2.rowcount >= 1:
                dt = cursor2.fetchone()
                if (dt['scheduled_start_time'] <= right_now and
                    dt['scheduled_end_time'] >= right_now):
                    continue

            if isinstance(row['last_time_ok'], datetime):
                last_time_ok = row['last_time_ok']
            else:
                if row['last_time_ok'].startswith('0000'):
                    row['last_time_ok'] = '1970-01-01 00:00:00'
                last_time_ok = datetime.strptime(row['last_time_ok'], '%Y-%m-%d %H:%M:%S')
            #last_check = datetime.strptime(row['last_check'], '%Y-%m-%d %H:%M:%S')
            last_check = row['last_check']

            duration = right_now - last_time_ok
            alerts.append(
                {
                    'alert_id': row['servicestatus_id'],
                    'object_id': row['service_object_id'],
                    'duration': str(duration).split('.')[0],
                    'servicestatus_id': row['servicestatus_id'],
                    'output': row['output'],
                    'hostname': row['hostname'],
                    'service': row['display_name'],
                    'state': row['current_state'],
                    'status': nagios_status[row['current_state']],
                    'host_object_id': row['host_object_id'],
                    'service_object_id': row['service_object_id'],
                    'last_check': last_check.strftime('%s')
                }
            )

    return {
        'alerts_count': len(alerts),
        'alerts': alerts
    }


def nagios_command(cmd_file, command, **kwargs):
    """
    Takes a file object as argument for the nagios rw command file. First
    argument is the command but the rest are key word args.
    """

    # Some helpful notes about nagios commands.
    # https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/3/en/extcommands.html
    cmd = '[{ts}] {cmd};'.format(
        ts=datetime.now().strftime('%s'),
        cmd=command
    )

    if command.lower().startswith('acknowledge_host'):
        cmd += '{host};{sticky};{notify};{persistent};{author};{comment}\n'.format(
            **kwargs
        )
    elif command.lower().startswith('acknowledge_svc'):
        cmd += '{host};{service};{sticky};{notify};{persistent};{author};{comment}\n'.format(
            **kwargs
        )
    elif command.lower().startswith('schedule_svc_downtime'):
        cmd += '{host};{service};{start};{end};{fixed};{trigger};{duration};{author};{comment}\n'.format(
            **kwargs
        )
    elif command.lower().startswith('schedule_host_downtime'):
        cmd += '{host};{start};{end};{fixed};{trigger};{duration};{author};{comment}\n'.format(
            **kwargs
        )
    elif command.lower().startswith('schedule_forced_host_check'):
        cmd += '{host};{start}\n'.format(
            **kwargs
        )
    elif command.lower().startswith('schedule_forced_svc_check'):
        cmd += '{host};{service};{start}\n'.format(
            **kwargs
        )
    else:
        raise ValueError('Incorrect command argument')
    cmd_file.write(cmd)


def nagios_endtime(hour, minute=0):
    """
    Calculate tomorrows datetime at hour:minute
    """

    now = datetime.now()
    now_time = now.time()
    if now_time >= time(23,59) or now_time <= time(hour, minute):
        tomorrow = now + timedelta(hours=hour, minutes=minute)
    else:
        tomorrow = now + timedelta(days=1, hours=hour, minutes=minute)

    tomorrow = tomorrow.replace(hour=0, minute=0)
    return tomorrow + timedelta(hours=hour, minutes=minute)


def get_username(auth_value):
    """
    Take Authorization header value and decode username
    """

    # If we're in debug mode, local dev server for example
    if config.get('mondash', 'debug') and not auth_value:
        return 'No username'

    encoded = auth_value.replace('Basic ', '', 1)
    decoded = b64decode(encoded).decode('utf-8')
    pair = decoded.split(':')
    return pair[0]


def list_readonly_users():
    usernames = []

    try:
        ro_users = config.get('mondash', 'readonly_users')
    except Exception as e:
        ro_users = ''

    for user in ro_users.split(','):
        usernames.append(user.strip(' '))

    return usernames


@route('/static/<path:path>')
def server_static(path):
    return static_file(path, root='./static')


@route('/')
def index():
    return static_file('mondash.html', root='./static')


@post('/alert/nagios/<alert_type>/<object_id:int>/<command>')
@require_write_access
def alert_nagios_handle(alert_type, object_id, command):
    """
    Command handling for Nagios alerts.
    """

    try:
        username = get_username(request.headers.get('Authorization'))
    except Exception as e:
        l.debug(str(e))
        username = None
        pass

    alerts = get_nagios_alerts(alert_type)
    if alerts.get('alerts_count') <= 0:
        raise HTTPError(status=404, body='No alerts found')

    for alert in alerts.get('alerts'):
        if alert.get('object_id') == object_id:
            nagios_rw_socket = config.get('ndo2db', 'nagios_rw_socket')

            # Basic arguments that go with any external command
            nag_cmd_args = {
                'host': alert.get('hostname'),
                'author': username,
                'comment': 'Command sent by {user} through Mondash dashboard'.format(
                    user=username
                )
            }

            # Add more arguments based on which external command is being used
            if alert_type == 'host':
                nagios_type = 'HOST'
            elif alert_type == 'service':
                nagios_type = 'SVC'
                nag_cmd_args['service'] = alert.get('service')
            else:
                raise HTTPError(status=404, body='Alert type not found')

            if command == 'acknowledge':
                nagios_cmd = 'ACKNOWLEDGE_{type}_PROBLEM'.format(
                    type=nagios_type
                )
                nag_cmd_args['sticky'] = 2
                nag_cmd_args['notify'] = 0
                nag_cmd_args['persistent'] = 1
            elif command == 'disable':
                nagios_cmd = 'SCHEDULE_{type}_DOWNTIME'.format(
                    type=nagios_type
                )
                nag_cmd_args['start'] = datetime.now().strftime('%s')
                end_time = nagios_endtime(7, 30)
                nag_cmd_args['end'] = end_time.strftime('%s')
                nag_cmd_args['fixed'] = 1
                nag_cmd_args['trigger'] = 0
                duration = end_time - datetime.now()
                nag_cmd_args['duration'] = duration.seconds
            elif command == 'force':
                nagios_cmd = 'SCHEDULE_FORCED_{type}_CHECK'.format(
                    type=nagios_type
                )
                nag_cmd_args['start'] = datetime.now().strftime('%s')
            else:
                raise HTTPError(status=404, body='Command not recognized')

            try:
                rw_socket = open(nagios_rw_socket, 'a')
                nagios_command(
                    rw_socket,
                    nagios_cmd,
                    **nag_cmd_args
                )
                rw_socket.close()
            except Exception as e:
                l.debug(str(e))
                raise HTTPError(
                    status=500,
                    body=str(e)
                )



@route('/alert/nagios/service')
def alert_nagios_service():
    response.content_type = 'application/json'
    nagios_alerts = get_nagios_alerts('service')

    get_params = request.query_string.split('&')
    if 'pp' in get_params:
        return dumps(
            nagios_alerts,
            sort_keys=True,
            indent=4
        )
    return dumps(nagios_alerts)


@route('/alert/nagios/host')
def alert_nagios_host():
    response.content_type = 'application/json'
    nagios_alerts = get_nagios_alerts('host')

    get_params = request.query_string.split('&')
    if 'pp' in get_params:
        return dumps(
            nagios_alerts,
            sort_keys=True,
            indent=4
        )
    return dumps(nagios_alerts)


if __name__ == '__main__':
    run(
        host=config.get('mondash', 'listen_host'),
        port=config.getint('mondash', 'listen_port')
    )
    debug(config.getbool('mondash', 'debug'))
else:
    application = default_app()
