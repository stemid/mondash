var mondashApp = angular.module('mondashApp', []);

// Configuration constants
mondashApp.constant('appConfig', {
    'output_cutoff': 40,
    'nagiosHostAlertUrl': '/alert/nagios/host',
    'nagiosServiceAlertUrl': '/alert/nagios/service',
    'monitorscoutMonitorAlertUrl': '/alert/monitorscout/monitor',
    'monitorscoutDeviceAlertUrl': '/alert/monitorscout/device'
});

mondashApp.factory('AlertsPoller', function ($http, $timeout) {
    var data = {
        resp: {},
        alert_count: 0,
        poll_count: 0,
        dict: {},
        error: ''
    };
    var poll_count=0;

    var poller = function (url, successCB, errorCB) {
        var alert_count=0;
        poll_count++;

        $http.get(url).then(function (responseData) {
            data.poll_count = poll_count;
            data.resp = responseData.data;

            // turn into hash table
            var data_dict = {};
            var alertObj = {}
            for (var i=0;i<data.resp.alerts.length;i++) {
                alert_count++;
                alertObj = data.resp.alerts[i];
                alertObj['buttons'] = {
                    acknowledge: 'Acknowledge',
                    disable: 'Disable',
                    force: 'Force'
                };
                data_dict[data.resp.alerts[i].alert_id] = alertObj;
            }

            data.alert_count = alert_count;
            data.dict = data_dict;
            successCB(data);
            $timeout(function () {poller(url, successCB, errorCB);}, 5000);
        }, function (responseData) {
            var data_dict = {};
            data.dict = data_dict;

            data.error = 'Failed to fetch alerts from API, check connection and refresh page.';
            errorCB(data);
            $timeout(function () {poller(url, successCB, errorCB);}, 5000);
        });
    };

    return {
        poller: poller
    };
});


mondashApp.controller('nagiosHostAlertsCtrl', function nagiosHostAlertsCtrl($scope, $http, AlertsPoller, appConfig) {
    $scope.output_cutoff = appConfig.output_cutoff;

    AlertsPoller.poller(appConfig.nagiosHostAlertUrl, function(response) {
        $scope.alerts = response.dict;
        $scope.alert_count = response.alert_count;
        if (response.alert_count <= 0) {
            $scope.error = 'No alerts found';
        }
    }, function(response) {
        // Error callback
        $scope.alerts = {};
        $scope.alert_count = 0;
        $scope.error = response.error;
    });

    $scope.acknowledgeAlert = function (alertObj) {
        alertObj.buttons.acknowledge = 'Acknowledging';
        var alert_id = alertObj.object_id;

        $http.post(appConfig.nagiosHostAlertUrl+'/'+alert_id+'/acknowledge').then(function (responseData) {
            alertObj.buttons.acknowledge = 'Acknowledge';
        });
    };
    $scope.disableAlert = function (alertObj) {
        alertObj.buttons.disable = 'Disabling';
        var alert_id = alertObj.object_id;

        $http.post(appConfig.nagiosHostAlertUrl+'/'+alert_id+'/disable').then(function (responseData) {
            alertObj.buttons.disable = 'Disable';
        });
    };
    $scope.forceAlert = function (alertObj) {
        alertObj.buttons.force = 'Forcing';
        var alert_id = alertObj.object_id;

        $http.post(appConfig.nagiosHostAlertUrl+'/'+alert_id+'/force').then(function (responseData) {
            alertObj.buttons.force = 'Forcing';
        });
    };
});

mondashApp.controller('nagiosServiceAlertsCtrl', function nagiosServiceAlertsCtrl($scope, $http, AlertsPoller, appConfig) {
    $scope.output_cutoff = appConfig.output_cutoff;

    AlertsPoller.poller(appConfig.nagiosServiceAlertUrl, function(response) {
        $scope.alerts = response.dict;
        $scope.alert_count = response.alert_count;
        if (response.alert_count <= 0) {
            $scope.error = 'No alerts found';
        }
    }, function(response) {
        // Error callback
        $scope.alerts = {};
        $scope.alert_count = 0;
        $scope.error = response.error;
    });

    $scope.acknowledgeAlert = function (alertObj) {
        alertObj.buttons.acknowledge = 'Acknowledging';
        var alert_id = alertObj.object_id;

        $http.post(appConfig.nagiosServiceAlertUrl+'/'+alert_id+'/acknowledge').then(function (responseData) {
            alertObj.buttons.acknowledge = 'Acknowledge';
        });
    };
    $scope.disableAlert = function (alertObj) {
        alertObj.buttons.disable = 'Disabling';
        var alert_id = alertObj.object_id;

        $http.post(appConfig.nagiosServiceAlertUrl+'/'+alert_id+'/disable').then(function (responseData) {
            alertObj.buttons.disable = 'Disable';
        });
    };
    $scope.forceAlert = function (alertObj) {
        alertObj.buttons.force = 'Forcing';
        var alert_id = alertObj.object_id;

        $http.post(appConfig.nagiosServiceAlertUrl+'/'+alert_id+'/force').then(function (responseData) {
            alertObj.buttons.force = 'Forcing';
        });
    };

});

mondashApp.controller('monitorscoutMonitorAlertsCtrl', function monitorscoutMonitorAlertsCtrl($scope, $http, AlertsPoller, appConfig) {
    $scope.output_cutoff = appConfig.output_cutoff;

    $scope.acknowledgeAlert = function (alertObj) {
        alertObj.buttons.acknowledge = 'Acknowledging';
        var alert_id = alertObj.alert_id;
        var alert_type = alertObj.type;
        var org_id = alertObj.organisation;

        $http.post('alert/monitorscout/'+org_id+'/'+alert_type+'/'+alert_id+'/ack').then(function (responseData) {
            alertObj.buttons.acknowledge = 'Acknowledge';
        });
    };

    AlertsPoller.poller(appConfig.monitorscoutMonitorAlertUrl, function(response) {
        $scope.alerts = response.dict;
        $scope.alert_count = response.alert_count;
        if (response.alert_count <= 0) {
            $scope.error = 'No alerts found';
        }
    }, function(response) {
        // Error callback
        $scope.alerts = {};
        $scope.alert_count = 0;
        $scope.error = response.error;
    });
});

mondashApp.controller('monitorscoutDeviceAlertsCtrl', function monitorscoutDeviceAlertsCtrl($scope, $http, AlertsPoller, appConfig) {
    $scope.output_cutoff = appConfig.output_cutoff;

    $scope.acknowledgeAlert = function (alertObj) {
        alertObj.buttons.acknowledge = 'Acknowledging';
        var alert_id = alertObj.alert_id;
        var org_id = alertObj.organisation;

        $http.post('/alert/monitorscout/'+org_id+'/device/'+alert_id+'/close').then(function (responseData) {
            alertObj.buttons.acknowledge = 'Acknowledge';
        });
    };

    $scope.acknowledgeAllAlerts = function (alerts) {
        for (var alert in alerts) {
            $scope.acknowledgeAlert(alerts[alert]);
        }
    };

    AlertsPoller.poller(appConfig.monitorscoutDeviceAlertUrl, function(response) {
        $scope.alerts = response.dict;
        $scope.alert_count = response.alert_count;
        if (response.alert_count <= 0) {
            $scope.error = 'No alerts found';
        }
    }, function(response) {
        // Error callback
        $scope.alerts = {};
        $scope.alert_count = 0;
        $scope.error = response.error;
    });
});

mondashApp.run(function($http) {
    $http.defaults.headers.common["X-Custom-Header"] = "Angular.js";
});
