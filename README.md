# Mondash dashboard

>**Disclaimer** This was an internal project that I moved to Gitlab so if you're looking to use it, look elsewhere. It was written many years ago to fill a specific niche purpose and nothing else. - 2020-04-23
>Most of the project has been left intact, as-is when it was exported from an internal Gitlab. Nagios support was in fact removed before the export so it's no use to anyone.

Simple dashboard written to aggregate info from Nagios and Monitorscout in a mobile responsive webpage.

Consists of an API part called ``mondash.py`` and a client-side webpage in the folder webclient. The API is completely unprotected and must be behind HTTP AUTH and SSL to hide sensitive alert info.

# TODO

## User auth

Use bottles request.authorization with a decorator instead of relying on external auth. That way I can use request.authorization.username in the nagios commands.
